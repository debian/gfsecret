.TH GFSEC-SPLIT-GPG 1 2017-08-26 "gfsecret @PACKAGE_VERSION@" "Gfsecret Manual"

.SH NAME
gfsec-split-gpg \- Split a GnuPG primary private key

.SH SYNOPSIS
.SY gfsec-split
.RB [ \-h | --help ]
.RB [ \-v | --version ]
.RB [ \-u | --user-id
.IR id ]
.RB [ \-k | --keep ]
.RB [ \-c | --config
.IR file ]
.RB [ \-i | --interactive ]
.RB [ \-n | --threshold
.IR N ]
.B URI...
.YS

.SH DESCRIPTION
.PP
.B gfsec-split-gpg
is a wrapper script around
.B gfsec-split
to facilitate splitting a GnuPG private primary key into
a number of shares and dispatching the resulting shares
onto external storage supports.
.PP
The split key can then be temporarily reconstructed
.BR gfsec-use (1).

.SH OPTIONS
.TP
.BR -h ", " --help
Display the help message.
.TP
.BR -v ", " --version
Display the version message.
.TP
.BR -u ", " --user-id " " \fiuid\fR
Split the primary key associated with the specified
OpenPGP User ID. This option is only needed if the GnuPG
private keyring contains more than one primary private
key.
.TP
.BR -k ", " --keep
By default,
.B gfsec-split-gpg
will remove the key from the GnuPG keyring once it has
been successfully split. Use this option to prevent
the key from being removed.
.TP
.BR -c ", " --config " " \fifile\fR
Write the configuration file (allowing to reconstruct
the secret with
.BR gfsec-use (1)
) to the specified file. Default is
$XDG_CONFIG_HOME/gfsecret/masterkey.conf.
If \fiFILE\fR is a single filename without extension and
without a directory part, the file will be placed under
the $XDG_CONFIG_HOME/gfsecret directory with a .conf
extension.
.TP
.BR -i ", " --interactive
Present the user with an interactive menu to specify the
shares to create.
.TP
.BR -n ", " --threshold " " \fiN\fR
Specify the minimal number of shares required to
re-assemble the split file. Default is 2.

.SH NOTES
.PP
This script will only work with GnuPG 2.1 or higher.
It will abort before attempting anything if it cannot
detect a binary for the correct GnuPG version.

.SH EXAMPLE INVOCATION
.PP
.nf
gfsec-split-gpg alice \\
  file:///home/alice/.local/share/gfsecret/mykey \\
  label://USBSTICK/mykey \\
  mtp://RF2GB6X704P/Documents/mykey \\
.fi

.PP
The above example will split Alice's primary private key
into three shares: one on the local filesystem, one on the
USB mass storage device with the label \fIUSBSTICK\fR, and
one on the MTP-compliant device with the serial \fIRF2GB6X704P\fR.
A configuration file will be written in
.I $XDG_CONFIG_HOME/gfsecret/mysecret
allowing to automatically reconstruct the file with
.BR gfsec-use (1)
provided at least one of the two removable supports are
present.

.SH REPORTING BUGS
.PP
Report bugs to
.MT @PACKAGE_BUGREPORT@
Damien Goutte-Gattat
.ME .

.SH SEE ALSO
.BR gfsec-split (1),
.BR gfsec-use (1)

.SH COPYRIGHT
.ad l
.PP
Copyright \(co 2017 Damien Goutte-Gattat
.PP
This program is released under the GNU General Public License.
See the COPYING file in the source distribution or
.UR http://www.gnu.org/licenses/gpl.html
.UE .
