/*
 * xdg - Incenp.org Notch Library: XDG Base Directory implementation
 * Copyright (C) 2011 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ICP20110203_XDG_H
#define ICP20110203_XDG_H

typedef struct {
#define XDG_VAR(variable_name, member_name, default_value) \
    char *member_name;
#include "xdg-vars.h"
#undef XDG_VAR
} xdg_t;

extern xdg_t *xdg;

#ifdef __cplusplus
extern "C" {
#endif

xdg_t *
xdg_init(void);

xdg_t *
xdg_get(int);

#ifdef __cplusplus
}
#endif

#endif /* !ICP20110203_XDG_H */
