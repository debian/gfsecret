/*
 * splitstr - Incenp.org Notch Library: string-spliter module
 * Copyright (C) 2011 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <splitstr.h>
#include <errno.h>

#include <xmem.h>

/*
 * The following call
 *
 *   splitstr("to split this string", ' ', NULL);
 *
 * would return a pointer to a memory area whose contents would
 * look like as depicted:
 *
 * 0    4    8    12   16   20     23           29         34
 * +----+----+----+----+----+------+------------+----------+--------------+
 * | 20 | 23 | 29 | 34 |NULL|t|o|\0|s|p|l|i|t|\0|t|h|i|s|\0|s|t|r|i|n|g|\0|
 * +----+----+----+----+----+------+------------+----------+--------------+
 *
 * |<---------------------->|<------------------------------------------->|
 * |(n + 1) * sizeof(char *)|                    n + l                    |
 *
 * with `n' being the number of tokens, and `l' the total count of
 * characters in all tokens.
 *
 * The memory is allocated in a single block, so it can be freed by a
 * single call to free().
 */

char **
splitstr(const char *str, char delim, size_t *ntoken)
{
    const char *cursor;
    char *copy, **index;
    size_t n, l;
    int is_token;

    if ( ! str ) {
        errno = EINVAL;
        return NULL;
    }

    /* First parsing to compute the amount of memory needed. */
    for ( n = l = is_token = 0, cursor = str; *cursor; cursor++ ) {
        if ( *cursor != delim ) {
            if ( ! is_token ) {
                is_token = 1;
                n += 1;
            }
            l += 1;
        }
        else
            is_token = 0;
    }

    index = xmalloc((n + 1) * sizeof(char *) + l + n);
    copy = (char *)(index + (n + 1));

    /* Second parsing to copy the tokens in the buffer. */
    for ( n = l = is_token = 0, cursor = str; *cursor; cursor++ ) {
        if ( *cursor != delim ) {
            if ( ! is_token ) {
                is_token = 1;
                index[n++] = copy;
            }
            *copy++ = *cursor;
        }
        else {
            is_token = 0;
            if ( n && *(copy - 1) )
                *copy++ = '\0';
        }
    }
    *copy = '\0';
    index[n] = NULL;

    if ( ntoken )
        *ntoken = n;

    return index;
}
