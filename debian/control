Source: gfsecret
Section: utils
Priority: optional
Maintainer: Thomas Perret <thomas.perret@phyx.fr>
Build-Depends:
 debhelper-compat (= 13),
 libgfshare-dev,
 libgcrypt20-dev,
 libglib2.0-dev,
 libmtp-dev,
 libcurl4-openssl-dev | libcurl4-gnutls-dev | libcurl4-nss-dev | libcurl-dev,
 texinfo,
Standards-Version: 4.7.0
Homepage: https://incenp.org/dvlpt/gfsecret.html
Vcs-Browser: https://salsa.debian.org/debian/gfsecret
Vcs-Git: https://salsa.debian.org/debian/gfsecret.git
Rules-Requires-Root: no

Package: gfsecret
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Tools to make secret sharing easier
 Gfsecret is a set of tools to facilitate secret sharing according to the
 Adi Shamir’s secret sharing scheme.
 .
 Two tools are provided: gfsec-split will split a file into several shares,
 and gfsec-use will reconstruct the original file from some of the shares.
 Both tools use the concept of a share URI, which is a way of describing a
 share with a URI-like syntax. The gfsec-split program uses them to know where
 to dispatch the generated shares, and gfsec-use uses them to know where to
 search for shares in order to reconstruct the original file.
 .
 The package also provide a script wrapper around gfsec-split: gfsec-split-gpg
 to facilitate splitting your GnuPG private primary key which can be combine
 with gfsec-use.
