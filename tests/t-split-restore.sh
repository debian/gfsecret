#!/bin/bash

../src/gfsec-split -k -c $$.conf Makefile \
    file://$PWD/$$.share file://$PWD/$$.share file://$PWD/$$.share
if [ $? != 0 ]; then
    echo "t-split-restore: gfsec-split failed" >&2
    exit 1
fi

../src/gfsec-use -c $$.conf -k -o $$.rebuilt
if [ $? != 0 ]; then
    echo "t-split-restore: gfsec-use failed" >&2
    exit 1
fi

if [ "$(md5sum Makefile | cut -d' ' -f1)" != "$(md5sum $$.rebuilt | cut -d' ' -f1)" ]; then
    echo "t-split-restore: Reconstructed file differs from the original file"
    exit 1
fi

rm -f $$.*

exit 0
