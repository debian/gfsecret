/*
 * gfsecret - Secret sharing tools
 * Copyright (C) 2021 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "secret.h"
#include "secretcfg.h"


struct test {
    const char     *uri;
    int             rc;
    gfsec_scheme_t  scheme;
    const char     *authority;
    const char     *path;
    unsigned        sharenr;
    char            is_full;
};

struct test tests[] = {

    /* Empty string. */
    { "", GFSEC_ERR_CONFIG_UNKNOWN_SCHEME },

    /* Not an URI. */
    { "asdfjklm", GFSEC_ERR_CONFIG_UNKNOWN_SCHEME },

    /* Missing authority/path separator. */
    { "file://", GFSEC_ERR_CONFIG_INVALID_URI },
    { "file://authority", GFSEC_ERR_CONFIG_INVALID_URI },

    /* Recognizing the valid schemes. */
    { "file:///path/to/share.123",
        0, GFSEC_SCHEME_FILE, NULL, "/path/to/share.123", 123, 0 },
    { "uuid://UUID/path/to/share.123",
        0, GFSEC_SCHEME_UUID, "UUID", "/path/to/share.123", 123, 0 },
    { "mtp://SERIAL/path/to/share.123",
        0, GFSEC_SCHEME_MTP, "SERIAL", "/path/to/share.123", 123, 0 },
    { "label://LABEL/path/to/share.123",
        0, GFSEC_SCHEME_LABEL, "LABEL", "/path/to/share.123", 123, 0 },
    { "http://HOST/path/to/share.123",
        0, GFSEC_SCHEME_HTTP, "HOST", "/path/to/share.123", 123, 0 },
    { "https://HOST/path/to/share.123",
        0, GFSEC_SCHEME_HTTPS, "HOST", "/path/to/share.123", 123, 0 },
    { "scheme:///path/to/share.123", GFSEC_ERR_CONFIG_UNKNOWN_SCHEME },

    /* Empty authority. */
    { "uuid:///", GFSEC_ERR_CONFIG_MISSING_AUTHORITY },
    { "mtp:///", GFSEC_ERR_CONFIG_MISSING_AUTHORITY },
    { "label:///", GFSEC_ERR_CONFIG_MISSING_AUTHORITY },
    { "http:///", GFSEC_ERR_CONFIG_MISSING_AUTHORITY },
    { "https:///", GFSEC_ERR_CONFIG_MISSING_AUTHORITY },

    /* Non-localhost authority for file-based URIs. */
    { "file://localhost/path/to/share.123",
        0, GFSEC_SCHEME_FILE, "localhost", "/path/to/share.123", 123, 0 },
    { "file://abc/path/to/share.123", GFSEC_ERR_CONFIG_NONLOCAL_FILE_URI },
    { "file://abcdefghjklm/path/to/share.123", GFSEC_ERR_CONFIG_NONLOCAL_FILE_URI },
    { "file://localhostabc/path/to/share.123", GFSEC_ERR_CONFIG_NONLOCAL_FILE_URI },

    /* Empty path. */
    { "file:///", GFSEC_ERR_CONFIG_INVALID_URI_PATH },
    { "file:///?param=value", GFSEC_ERR_CONFIG_INVALID_URI_PATH },

    /* Path ending with a directory separator. */
    { "file:///path/", GFSEC_ERR_CONFIG_INVALID_URI_PATH },
    { "file:///path/?param=value", GFSEC_ERR_CONFIG_INVALID_URI_PATH },

    /* Several parameters. */
    { "file:///path/to/share.123?param1=val1&param2=val2&param3=val3",
        0, GFSEC_SCHEME_FILE, NULL, "/path/to/share.123", 123, 0 },
    { "file:///path/to/share.123?param1=val&share=no&param3=val3",
        0, GFSEC_SCHEME_FILE, NULL, "/path/to/share.123", 0, 1 },

    /* Empty parameters. */
    { "file:///path/to/share.123?param1=val&&share=124",
        0, GFSEC_SCHEME_FILE, NULL, "/path/to/share.123", 124, 0 },
    { "file:///path/to/share.123?&share=124&",
        0, GFSEC_SCHEME_FILE, NULL, "/path/to/share.123", 124, 0 },

    /* Parameter with no value. */
    { "file:///path/to/share.123?param", GFSEC_ERR_CONFIG_INVALID_URI_PARAM },
    { "file:///path/to/share.123?param1=val1&param2",
        GFSEC_ERR_CONFIG_INVALID_URI_PARAM },
    { "file:///path/to/share.123?param1=val1&param2&param3=val",
        GFSEC_ERR_CONFIG_INVALID_URI_PARAM },

    /* Parameter with empty value. */
    { "file:///path/to/share.123?param=", GFSEC_ERR_CONFIG_INVALID_URI_PARAM },
    { "file:///path/to/share.123?param1=&param2=val", GFSEC_ERR_CONFIG_INVALID_URI_PARAM },

    /* Handling the share parameter. */
    { "file:///path/to/share?share=no",
        0, GFSEC_SCHEME_FILE, NULL, "/path/to/share", 0, 1 },
    { "file:///path/to/share.123?share=no",
        0, GFSEC_SCHEME_FILE, NULL, "/path/to/share.123", 0, 1 },
    { "file:///path/to/share?share=full",
        0, GFSEC_SCHEME_FILE, NULL, "/path/to/share", 0, 1 },
    { "file:///path/to/share.123?share=full",
        0, GFSEC_SCHEME_FILE, NULL, "/path/to/share.123", 0, 1 },
    { "file:///path/to/share?share=invalid", GFSEC_ERR_CONFIG_INVALID_SHARENR },
    { "file:///path/to/share.123?share=invalid", GFSEC_ERR_CONFIG_INVALID_SHARENR },
    { "file:///path/to/share?share=124",
        0, GFSEC_SCHEME_FILE, NULL, "/path/to/share", 124, 0 },
    { "file:///path/to/share.123?share=124",
        0, GFSEC_SCHEME_FILE, NULL, "/path/to/share.123", 124, 0 },

    /* Hash with invalid length. */
    { "file:///path/to/share.123?sha256=decafbad", GFSEC_ERR_CONFIG_INVALID_HASH },
    { "file:///path/to/share.123?sha256="
        "decafbaddecafbaddecafbaddecafbaddecafbaddecafbaddecafbaddecafbadd",
        GFSEC_ERR_CONFIG_INVALID_HASH },
    { "file:///path/to/share.123?sha256="
        "decafbaddecafbaddecafbaddecafbaddecafbaddecafbaddecafbaddecafbad",
        0, GFSEC_SCHEME_FILE, NULL, "/path/to/share.123", 123, 0 },

    /* Hash with invalid characters. */
    { "file:///path/to/share.123?sha256="
        "decafbaddecafLaddecafbaddecafbaddecafbaddecafbaddecafbaddecafbad",
        GFSEC_ERR_CONFIG_INVALID_HASH },

    /* Invalid or missing share number in path. */
    { "file:///path/to/share", GFSEC_ERR_CONFIG_MISSING_SHARENR },
    { "file:///path/to/share.2", GFSEC_ERR_CONFIG_MISSING_SHARENR },
    { "file:///path/to/share.000", GFSEC_ERR_CONFIG_MISSING_SHARENR },
    { "file:///path/to/share.256", GFSEC_ERR_CONFIG_MISSING_SHARENR },
    { "file:///path/to/share.abc", GFSEC_ERR_CONFIG_MISSING_SHARENR },

    { NULL }
};

unsigned testnr;
unsigned sum;

static void
fail(const char *msg, ...)
{
    va_list ap;

    va_start(ap, msg);
    fprintf(stderr, "Test #%u: ", testnr);
    vfprintf(stderr, msg, ap);
    va_end(ap);

    sum += 1;
}


int
main(int argc, char **argv)
{
    int rc;
    gfsec_secret_t *secret;
    gfsec_share_t *share;
    struct test *test = tests;

    testnr = 1;
    sum = 0;

    while ( test->uri ) {
        secret = gfsec_secret_new();

        rc = gfsec_parse_uri(test->uri, secret, 0);
        if ( rc != test->rc )
            fail("expected code %d, got %d\n", test->rc, rc);

        if ( rc == 0 ) {
            share = secret->shares[0];

            if ( share->scheme != test->scheme )
                fail("expected scheme %u, got %u\n", test->scheme, share->scheme);

            if ( test->authority && strcmp(share->authority, test->authority) != 0 )
                fail("expected authority '%s', got '%s'\n", test->authority, share->authority);

            if ( strcmp(share->path, test->path) != 0 )
                fail("expected path '%s', got '%s'\n", test->path, share->path);

            if ( share->number != test->sharenr )
                fail("expected share number %u, got %u\n", test->sharenr, share->number);
        }

        gfsec_secret_free(secret);

        test += 1;
        testnr += 1;
    }

    return sum == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}
