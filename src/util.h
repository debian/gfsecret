/*
 * gfsec - Secret sharing tools
 * Copyright (C) 2016,2020 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ICP20160205_UTIL_H
#define ICP20160205_UTIL_H

#include <stdio.h>
#include <stdlib.h>

#ifdef __cpluscplus
extern "C" {
#endif

int
file_exists(const char *);

long
get_file_size(FILE *f);

unsigned char *
read_file(const char *, size_t *, size_t);

int
write_file(const char *, const unsigned char *, size_t);

char *
get_file_basename(const char *);

int
makedir(const char *, int);

#ifdef __cplusplus
}
#endif

#endif /* !ICP20160205_UTIL_H */
