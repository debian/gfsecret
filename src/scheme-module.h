/*
 * gfsecret - Secret sharing tools
 * Copyright (C) 2016 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ICP20160820_SCHEME_MODULE_H
#define ICP20160820_SCHEME_MODULE_H

#include <stdlib.h>

#include "schemes.h"
#include "support.h"

typedef enum gfsec_scheme_status {
    GFSEC_SCHEME_STATUS_SUCCESS = 0,
    GFSEC_SCHEME_STATUS_UNSUPPORTED_SCHEME = -1,
    GFSEC_SCHEME_STATUS_ERROR = -2
} gfsec_scheme_status_t;

#ifdef __cplusplus
extern "C" {
#endif

void
gfsec_scheme_module_init(void);

int
gfsec_scheme_module_get_file(gfsec_scheme_t,
                             const char *,
                             const char *,
                             unsigned char **,
                             size_t *);

int
gfsec_scheme_module_put_file(gfsec_scheme_t,
                             const char *,
                             const char *,
                             unsigned char *,
                             size_t);

gfsec_supports_list_t *
gfsec_scheme_module_get_supports(void);

#ifdef __cpluscplus
}
#endif

#endif /* !ICP20160820_SCHEME_MODULE_H */
