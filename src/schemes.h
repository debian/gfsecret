/*
 * gfsecret - Secret sharing tools
 * Copyright (C) 2016,2021 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ICP20160820_SCHEMES_H
#define ICP20160820_SCHEMES_H

typedef enum gfsec_scheme {
    GFSEC_SCHEME_FILE = 0,
    GFSEC_SCHEME_UUID,
    GFSEC_SCHEME_LABEL,
    GFSEC_SCHEME_MTP,
    GFSEC_SCHEME_HTTP,
    GFSEC_SCHEME_HTTPS
} gfsec_scheme_t;

#define GFSEC_SCHEME_MAX 4

#endif /* !ICP20160820_SCHEMES_H */
