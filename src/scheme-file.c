/*
 * gfsecret - Secret sharing tools
 * Copyright (C) 2016,2017 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include "scheme-file.h"
#include "scheme-module.h"
#include "util.h"

#include <xmem.h>

int
gfsec_scheme_file_get_file(gfsec_scheme_t   scheme,
                           const char      *authority,
                           const char      *path,
                           unsigned char  **buffer,
                           size_t          *len)
{
    if ( scheme != GFSEC_SCHEME_FILE )
        return GFSEC_SCHEME_STATUS_UNSUPPORTED_SCHEME;

    if ( authority && *authority && strcmp(authority, "localhost") != 0 )
        return GFSEC_SCHEME_STATUS_ERROR;

    if ( (*buffer = read_file(path, len, GFSEC_SECRET_MAX_SIZE)) )
        return GFSEC_SCHEME_STATUS_SUCCESS;
    else
        return GFSEC_SCHEME_STATUS_ERROR;
}

int
gfsec_scheme_file_put_file(gfsec_scheme_t   scheme,
                           const char      *authority,
                           const char      *path,
                           unsigned char   *buffer,
                           size_t           len)
{
    if ( scheme != GFSEC_SCHEME_FILE )
        return GFSEC_SCHEME_STATUS_UNSUPPORTED_SCHEME;

    if ( authority && *authority && strcmp(authority, "localhost") != 0 )
        return GFSEC_SCHEME_STATUS_ERROR;

    if ( write_file(path, buffer, len) != -1 )
        return GFSEC_SCHEME_STATUS_SUCCESS;
    else
        return GFSEC_SCHEME_STATUS_ERROR;
}

int
gfsec_scheme_file_get_supports(gfsec_supports_list_t *list)
{
    gfsec_support_add(list, GFSEC_SCHEME_FILE, xstrdup(""), xstrdup(_("Local filesystem")));

    return 1;
}
