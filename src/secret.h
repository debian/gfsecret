/*
 * gfsecret - Secret sharing tools
 * Copyright (C) 2016,2017,2020,2021 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ICP20160807_SECRET_H
#define ICP20160807_SECRET_H

enum gfsec_error {
#define GFSEC_ERROR(symbol, value, message) \
    GFSEC_ERR_##symbol = value,
#include "gfsec-errors.h"
#undef GFSEC_ERROR
};

#define GFSEC_SHARE_NUMBER_FULL         0
#define GFSEC_SHARE_NUMBER_AUTOASSIGN   256

#include "schemes.h"

/**
 * Represents a share of a split secret.
 */
typedef struct gfsec_share {
    /*
     * This must be set to a value between 1 and 255 (inclusive).
     * A value of 0 indicates this share actually contains the whole
     * secret. A value of GFSEC_SHARE_NUMBER_AUTOASSIGN means the
     * share number will be automatically assigned later.
     */
    unsigned short  number;

    /*
     * The following fields describe the location where the share
     * data is to be found, as a scheme://authority/path triplet.
     */
    gfsec_scheme_t  scheme;
    char           *authority;
    char           *path;

    size_t          len;    /* Size of the share's data. */
    unsigned char  *data;   /* The actual share's data. */
    unsigned char  *hash;   /* A SHA-256 hash of the data. */
} gfsec_share_t;

#define gfsec_share_is_full(share)  (share->number == GFSEC_SHARE_NUMBER_FULL)

/**
 * Represents a split secret (or a secret to be split).
 */
typedef struct gfsec_secret {
    /* The location of the reconstituted secret. */
    char           *filename;

    /* The restore/delete commands. */
    char           *restore;
    char           *destroy;

    /* The secret's actual data. */
    unsigned char  *data;
    size_t          len;

    /* The number of shares this secret is split into. */
    unsigned char   n_shares;

    /* The current size of the \c shares array. */
    unsigned char   max_shares;

    /* The number of shares needed to reconstitute the secret. */
    unsigned char   threshold;

    /* The shares this secret is split into. */
    gfsec_share_t **shares;

    /* May point to one of the shares in the \c shares array
     * containing the whole secret. */
    gfsec_share_t  *full_share;

    /* If non-zero, indicates the secret can be reconstituted. */
    unsigned        can_combine;
} gfsec_secret_t;

#ifdef __cpluscplus
extern "C" {
#endif

gfsec_share_t *
gfsec_share_new(void);

void
gfsec_share_set_info(gfsec_share_t *,
                     unsigned short,
                     gfsec_scheme_t,
                     const char *,
                     const char *,
                     const unsigned char *);

int
gfsec_share_set_data(gfsec_share_t *,
                     unsigned char *,
                     size_t);

int
gfsec_share_get_uri(gfsec_share_t *, char *, size_t);

void
gfsec_share_free(gfsec_share_t *);

gfsec_secret_t *
gfsec_secret_new(void);

int
gfsec_secret_set_secret_file(gfsec_secret_t *, const char *, const char *);

int
gfsec_secret_add_share(gfsec_secret_t *, gfsec_share_t *);

int
gfsec_secret_can_combine(gfsec_secret_t *);

int
gfsec_secret_combine(gfsec_secret_t *);

int
gfsec_secret_split(gfsec_secret_t *, unsigned char);

void
gfsec_secret_free(gfsec_secret_t *);

const char *
gfsec_error_string(int);

#ifdef __cplusplus
}
#endif

#endif /* !ICP20160807_SECRET_H */
