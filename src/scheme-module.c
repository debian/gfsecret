/*
 * gfsecret - Secret sharing tools
 * Copyright (C) 2016,2021 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "scheme-module.h"
#include "scheme-file.h"
#include "scheme-libmtp.h"
#include "scheme-gio.h"
#include "scheme-libcurl.h"

#ifdef HAVE_LIBMTP
#include <libmtp.h>
#endif

#ifdef HAVE_LIBCURL
#include <curl/curl.h>
#endif

typedef int (*fn_get_file)(gfsec_scheme_t,
                           const char *,
                           const char *,
                           unsigned char **,
                           size_t *);

typedef int (*fn_put_file)(gfsec_scheme_t,
                           const char *,
                           const char *,
                           unsigned char *,
                           size_t);

typedef int (*fn_get_supp)(gfsec_supports_list_t *);

typedef struct module {
    int         present;
    fn_get_file get_file;
    fn_put_file put_file;
    fn_get_supp get_supp;
} module_t;

static module_t *modules;

/**
 * Initializes the module system.
 * This function should be called once, before any other
 * function from this module.
 */
void
gfsec_scheme_module_init(void)
{
    static module_t mods[5];
    int n = 0;

    mods[n].get_file = gfsec_scheme_file_get_file;
    mods[n].put_file = gfsec_scheme_file_put_file;
    mods[n].get_supp = gfsec_scheme_file_get_supports;
    mods[n++].present = 1;

#ifdef HAVE_LIBMTP
    LIBMTP_Init();
    mods[n].get_file = gfsec_scheme_libmtp_get_file;
    mods[n].put_file = gfsec_scheme_libmtp_put_file;
    mods[n].get_supp = gfsec_scheme_libmtp_get_supports;
    mods[n++].present = 1;
#endif

#ifdef HAVE_GIO
    mods[n].get_file = gfsec_scheme_gio_get_file;
    mods[n].put_file = gfsec_scheme_gio_put_file;
    mods[n].get_supp = gfsec_scheme_gio_get_supports;
    mods[n++].present = 1;
#endif

#ifdef HAVE_LIBCURL
    if ( curl_global_init(CURL_GLOBAL_DEFAULT) == 0 ) {
        mods[n].get_file = gfsec_scheme_libcurl_get_file;
        mods[n].put_file = gfsec_scheme_libcurl_put_file;
        mods[n].get_supp = gfsec_scheme_libcurl_get_supports;
        mods[n++].present = 1;
    }
#endif

    mods[n++].present = 0;

    modules = mods;
}

/**
 * Attempts to retrieve a file's contents.
 * This function will successively ask all available modules
 * to retrieve the file at the specified location.
 *
 * @param scheme[in]    The URI scheme.
 * @param authority[in] The URI authority part.
 * @param path[in]      The URI path.
 * @param buffer[out]   A pointer to a buffer to store the file
 *                      contents; will be automatically allocated.
 * @param len[out]      The address where the count of retrieved
 *                      bytes will be stored.
 *
 * @return
 * - GFSEC_SCHEME_STATUS_SUCCESS if successful;
 * - GFSEC_SCHEME_STATUS_UNSUPPORTED_SCHEME if no module is
 *   available to handle the URI scheme;
 * - GFSEC_SCHEME_STATUS_ERROR if a module supported the
 *   URI scheme but an error occured during retrieval.
 */
int
gfsec_scheme_module_get_file(gfsec_scheme_t   scheme,
                             const char      *authority,
                             const char      *path,
                             unsigned char  **buffer,
                             size_t          *len)
{
    module_t *mod;
    int rc = GFSEC_SCHEME_STATUS_UNSUPPORTED_SCHEME;

    for ( mod = modules; mod->present; mod++ ) {
        if ( mod->get_file ) {
            rc = mod->get_file(scheme, authority, path, buffer, len);
            if ( rc == GFSEC_SCHEME_STATUS_SUCCESS )
                break;
        }
    }

    return rc;
}

/**
 * Attempts to write a file to the specified URI.
 *
 * @param scheme    The URI scheme.
 * @param authority The URI authority part.
 * @param path      The URI path.
 * @param buffer    The data to write.
 * @param len       The size of the data buffer.
 *
 * @return
 * - GFSEC_SCHEME_STATUS_SUCCESS if successful;
 * - GFSEC_SCHEME_STATUS_UNSUPPORTED_SCHEME if no module is
 *   available to handle the URI scheme;
 * - GFSEC_SCHEME_STATUS_ERROR if a module supported the
 *   URI scheme but an error occured during the operation.
 */
int
gfsec_scheme_module_put_file(gfsec_scheme_t   scheme,
                             const char      *authority,
                             const char      *path,
                             unsigned char   *buffer,
                             size_t           len)
{
    module_t *mod;
    int rc = GFSEC_SCHEME_STATUS_UNSUPPORTED_SCHEME;

    for ( mod = modules; mod->present; mod++ ) {
        if ( mod->put_file ) {
            rc = mod->put_file(scheme, authority, path, buffer, len);
            if ( rc == GFSEC_SCHEME_STATUS_SUCCESS )
                break;
        }
    }

    return rc;
}

/**
 * Get a list of currently available supports.
 *
 * @return A gfsec_supports_list_t object containing all available
 * supports.
 */
gfsec_supports_list_t *
gfsec_scheme_module_get_supports(void)
{
    gfsec_supports_list_t *list = gfsec_support_new_list();
    module_t *mod;

    for ( mod = modules; mod->present; mod++ ) {
        if ( mod->get_supp )
            mod->get_supp(list);
    }

    return list;
}
