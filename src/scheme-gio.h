/*
 * gfsec - Secret sharing tools
 * Copyright (C) 2016 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ICP20160207_SCHEME_GIO_H
#define ICP20160207_SCHEME_GIO_H

#include "schemes.h"
#include "support.h"

#ifdef __cpluscplus
extern "C" {
#endif

int
gfsec_scheme_gio_get_file(gfsec_scheme_t,
                          const char *,
                          const char *,
                          unsigned char **,
                          size_t *);

int
gfsec_scheme_gio_put_file(gfsec_scheme_t,
                          const char *,
                          const char *,
                          unsigned char *,
                          size_t);

int
gfsec_scheme_gio_get_supports(gfsec_supports_list_t *);

#ifdef __cplusplus
}
#endif

#endif /* !ICP20160207_SCHEME_GIO_H */
