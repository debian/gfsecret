/*
 * gfsecret - Secret sharing tools
 * Copyright (C) 2016 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "support.h"

#include <xmem.h>

/**
 * Create a new list of supports.
 */
gfsec_supports_list_t *
gfsec_support_new_list(void)
{
    gfsec_supports_list_t *list;

    list = xmalloc(sizeof(*list));
    list->supports = xmalloc(10 * sizeof(gfsec_support_t));
    list->size = 10;
    list->count = 0;

    return list;
}

/**
 * Destroy a list of supports and release all associated memory.
 *
 * @param list The list to destroy.
 */
void
gfsec_support_destroy_list(gfsec_supports_list_t *list)
{
    unsigned n;

    for ( n = 0; n < list->count; n++ ) {
        free(list->supports[n].authority);
        free(list->supports[n].description);
    }

    free(list->supports);
    free(list);
}

/**
 * Add a new support to a list of supports.
 *
 * @param list The list to modify.
 * @param scheme The scheme associated to the support.
 * @param authority The authority refering to the support.
 * @param description A free-form description of the support.
 *
 * @note The string parameters will be owned by the list and freed
 * when the gfsec_support_destroy_list function is called.
 */
void
gfsec_support_add(gfsec_supports_list_t *list,
                  gfsec_scheme_t         scheme,
                  char                  *authority,
                  char                  *description)
{
    if ( list->count >= list->size ) {
        list->size += 10;
        list->supports = xrealloc(list->supports, list->size * sizeof(gfsec_support_t));
    }

    list->supports[list->count].scheme = scheme;
    list->supports[list->count].authority = authority;
    list->supports[list->count].description = description;

    list->count += 1;
}
