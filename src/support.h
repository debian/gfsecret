/*
 * gfsecret - Secret sharing tools
 * Copyright (C) 2016 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ICP20161015_SUPPORT_H
#define ICP20161015_SUPPORT_H

#include "schemes.h"

typedef struct gfsec_support {
    gfsec_scheme_t  scheme;
    char           *authority;
    char           *description;
} gfsec_support_t;

typedef struct gfsec_supports_list {
    gfsec_support_t    *supports;
    size_t              size;
    size_t              count;
} gfsec_supports_list_t;


#ifdef __cpluscplus
extern "C" {
#endif

gfsec_supports_list_t *
gfsec_support_new_list(void);

void
gfsec_support_destroy_list(gfsec_supports_list_t *);

void
gfsec_support_add(gfsec_supports_list_t *, gfsec_scheme_t, char *, char *);

#ifdef __cplusplus
}
#endif

#endif /* !ICP20161015_SUPPORT_H */
