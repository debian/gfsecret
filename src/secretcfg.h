/*
 * secretcfg - Secret sharing tools
 * Copyright (C) 2016 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ICP20160206_SECRETCFG_H
#define ICP20160206_SECRETCFG_H

#include "secret.h"

#ifdef __cpluscplus
extern "C" {
#endif

int
gfsec_parse_uri(const char *, gfsec_secret_t *, int);

int
gfsec_read_config(gfsec_secret_t **, const char *, unsigned int *);

int
gfsec_write_config(gfsec_secret_t *, const char *);

#ifdef __cplusplus
}
#endif

#endif /* !ICP20160206_SECRETCFG_H */
