/*
 * gfsecret - Secret sharing tools
 * Copyright (C) 2016,2017,2021 Damien Goutte-Gattat
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

GFSEC_ERROR(SUCCESS,                     0, _("Success"))
GFSEC_ERROR(SYSTEM_ERROR,               -1, strerror(errno))
GFSEC_ERROR(INVALID_CALL,               -2, _("A function was improperly called"))
GFSEC_ERROR(NOT_ENOUGH_SHARES,          -3, _("Not enough shares available"))
GFSEC_ERROR(TOO_MANY_SHARES,            -4, _("Too many shares"))
GFSEC_ERROR(SHARE_NOT_AVAILABLE,        -5, _("A share is not available"))
GFSEC_ERROR(INVALID_LENGTH,             -6, _("Shares have inconsistent lengths"))
GFSEC_ERROR(INVALID_SHARE,              -7, _("Checksum failed for share data"))
GFSEC_ERROR(CONFIG_LINE_TOO_LONG,       -9, _("Line too long"))
GFSEC_ERROR(CONFIG_INVALID_URI,        -10, _("Invalid share URI"))
GFSEC_ERROR(CONFIG_INVALID_HASH,       -11, _("Invalid hash value"))
GFSEC_ERROR(CONFIG_UNKNOWN_SCHEME,     -12, _("Unknown URI scheme"))
GFSEC_ERROR(CONFIG_INVALID_THRESHOLD,  -13, _("Invalid threshold"))
GFSEC_ERROR(CONFIG_INVALID_SHARENR,    -14, _("Invalid share parameter"))
GFSEC_ERROR(CONFIG_INVALID_URI_PATH,   -15, _("Invalid path in share URI"))
GFSEC_ERROR(CONFIG_INVALID_URI_PARAM,  -16, _("Invalid parameter in share URI"))
GFSEC_ERROR(CONFIG_MISSING_AUTHORITY,  -17, _("Missing authority in share URI"))
GFSEC_ERROR(CONFIG_MISSING_SHARENR,    -18, _("Missing share number"))
GFSEC_ERROR(CONFIG_NONLOCAL_FILE_URI,  -19, _("Invalid non-local file URI"))
